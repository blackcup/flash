<?php

namespace BlackCup\Flash;

class Action
{
    public $title;
    public $url;
    public $style;
    public $method;

    /**
     * Create a new Action instance.
     *
     * @param string $title
     * @param string $url
     * @param string $style (optional)
     * @param string $method (optional)
     */
    public function __construct($title, $url, $style = 'default', $method = 'GET')
    {
        $this->title = $title;
        $this->url = $url;
        $this->style = $style;
        $this->method = $method;
    }
}
