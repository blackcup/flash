<?php

namespace BlackCup\Flash;

class Message
{
    public $message;
    public $level;
    public $title;
    public $actions;

    /**
     * Create a new Message instance.
     *
     * @param string $message
     * @param string $level
     * @param string $title (optional)
     * @param array $actions (optional)
     */
    public function __construct($message, $level, $title = null, $actions = [])
    {
        $this->message = $message;
        $this->level = $level;
        $this->title = $title;
        $this->actions = $actions;
    }
}
