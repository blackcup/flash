<?php

namespace BlackCup\Flash;

use Illuminate\Contracts\Session\Session;

class FlashNotifier
{
    /**
     * The session writer.
     *
     * @var Session
     */
    protected $session;

    /**
     * The message being flashed.
     *
     * @var string
     */
    public $messages = [];

    /**
     * Create a new FlashNotifier instance.
     *
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
        $this->messages = $this->session->get('flash_notification', []);
    }

    /**
     * Flash an information message.
     *
     * @param  string $message
     * @param  string $title (optional)
     * @return $this
     */
    public function info($message, $title = null)
    {
        $this->message($message, 'info', $title);
    }

    /**
     * Flash a success message.
     *
     * @param  string $message
     * @param  string $title (optional)
     * @return $this
     */
    public function success($message, $title = null)
    {
        $this->message($message, 'success', $title);
    }

    /**
     * Flash an error message.
     *
     * @param  string $message
     * @param  string $title (optional)
     * @return $this
     */
    public function error($message, $title = null)
    {
        $this->message($message, 'danger', $title);
    }

    /**
     * Flash a warning message.
     *
     * @param  string $message
     * @param  string $title (optional)
     * @return $this
     */
    public function warning($message, $title = null)
    {
        $this->message($message, 'warning', $title);
    }

    /**
     * Flash a general message.
     *
     * @param  string $message
     * @param  string $level (optional)
     * @param  string $title (optional)
     * @return $this
     */
    public function message($message, $level = 'info', $title = null)
    {
        return $this->addMessage(new Message($message, $level, $title));
    }

    /**
     * Flash a new message to the session.
     *
     * @param  Message $message
     * @return $this
     */
    public function addMessage(Message $message)
    {
        array_push($this->messages, $message);

        $this->flash();

        return $this;
    }

    /**
     * Flash all messages to the session.
     */
    protected function flash()
    {
        $this->session->put('flash_notification', $this->messages);
    }
}
