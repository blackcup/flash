@foreach ((array) session('flash_notification') as $message)
    <div class="alert alert-{{ $message->level }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        @if ($message->title)
            <h4>{{ $message->title }}</h4>
        @endif

        <p>{{ $message->message }}</p>

        @if ($message->actions)
            <p></p>
        @endif

        @foreach ($message->actions as $action)
            @if (strcasecmp($action->method, 'GET') == 0)
                <a href="{{ $action->url }}" class="btn btn-{{ $action->style }}" title="{{ $action->title }}">{{ $action->title }}</a>
            @else
                <form action="{{ $action->url }}" method="POST" style="display: inline">
                    {{ method_field($action->method) }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-{{ $action->style }}" title="{{ $action->title }}">{{ $action->title }}</button>
                </form>
            @endif
        @endforeach

    </div>
@endforeach

{{ session()->forget('flash_notification') }}
